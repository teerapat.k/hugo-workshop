# Hugo Static Site Generator Workshop

# Table of Contents
1. [Installation](#Installation)
2. [Workshop](#Workshop)
3. [Assignment](#Assignment)

# Prerequisite
1. Hugo
2. Gitlab account


## Installation
[Install Hugo | Hugo](https://gohugo.io/getting-started/installing/)

## Workshop
### 1. Create New site

1. Create new site
	```console
	$ hugo new site <yoursite.com>
	```
	 cd to your site
	```console
	$ cd <yoursite.com>
	```

2. Initial Git to your site
	```console
	$ git init .
	```

### 2. Install Theme

1. Look up at  Bigspring [Bigspring Hugo Startup Theme | Hugo Themes](https://themes.gohugo.io/bigspring-hugo-startup-theme/)

2. Install to your site
	```console
	$ cd themes
	$ git clone https://github.com/themefisher/bigspring-hugo-startup-theme bigspring
	$ cd ..
	```

3. Copy example content to your site
	```console
	$ cp -R themes/bigspring/exampleSite/* .
	```
4. Start Hugo server for test view website
	```console
	$ hugo server
	```
### 3. Edit Website Informations
1. Edit _config.toml_ after change site will be live-reload

	Change
   	* baseURL
   	* title

### 4. Edit Contents
### 5. Generate Site
1. Generate site for deployment with command `hugo`
   ```console
	$ hugo
   ```
2. You can copy your `public` folder to deploy with any host


### 6. Build & Deploy with Gitlab Runner

1. Create file _.gitlab-ci.yml_

	```yml
      image: monachus/hugo

      variables:
        GIT_SUBMODULE_STRATEGY: recursive

      pages:
        script:
        - hugo
        artifacts:
          paths:
          - public
        only:
        - master
	```

2. Set up your Personal gitlab repository
`Repository Name: my-web`

	* Create new repository
	![create new repository](images/06/image_01.png)

	* Create blank project
	![](images/06/image_02.png)

	* Fill `Repository Name: my-website`
	![](images/06/image_03.png)

	* click `Create project`


1. Add your origin server to your local repository
   ```console
   $ git remote add origin https://gitlab.com/<YOURACCOUNT>/my-website.git
   ```

2. Add `.gitignore` file for ignore a `public` folder

	```
	public
	```

6. Add files to stage

   ```console
   $ git add .
   ```

5. Remove git sub-module in `themes/bigspring` folder and add again

	```console
	$ rm -Rf themes/bigspring/.git
	$ git rm --cached -f themes/bigspring
	$ git add .
	```

7. Commit & push to remote repository

	```console
	$ git commit -am "💖 First commit"
	$ git push origin master
	```

8. Wait until pipeline success ✅ and goto `Settings -> Pages`, copy your pages url example: `https://teerapat.k.gitlab.io/my-website`
   ![](images/08/image_01.png)

9. Back to your local work, Edit `config.toml` and change `baseURL` to your pages url

	```toml
	######################## default configuration ####################
 	baseURL = "https://<YOURACCOUNT>.gitlab.io/my-website"
	 .
	 .
	```

10. Add and Commit again
    ```console
	$ git add .
	$ git commit -am "🚀 Pages URL"
	$ git push origin master
	```
11. Wait until pipeline running success
	![](images/11/image_01.png)

	Go to `https://\<YOURACCOUNT\>.gitlab.io/my-website`

	![](images/11/image_02.png)

😎😎😎😎


- - - -

## Assignment
- [ ] Display “EN” content only
	<details><summary>Hints</summary>
	<pre>Remove “DE” Section</pre></details>


- [ ] Change Banner **Title** to your name
	<details><summary>Hints</summary>
	<pre>Edit config.toml</pre></details>

- [ ] Edit 1st feature with
	```
	name:  Clean Code
	icon: ✓
	content:  “With understandability comes readability, changeability and maintainability”
	```
	<details><summary>Hints</summary>
	<pre>icons: https://themify.me/themify-icons</pre></details>
- [ ] Add new image in Services with [Image](https://www.kindpng.com/picc/m/2-21277_powerbi-power-bi-dashboard-png-transparent-png.png)
	<details><summary>Hints</summary>
	<pre>Save image to static folder and Edit homepage.yaml to display</pre></details>
- [ ] Change copy right & licenses to your name
	<details><summary>Hints</summary>
	<pre>Edit config.toml</pre></details>
- [ ] Deploy to your repository
	<details><summary>Hints</summary>
	<pre>Commit and Push again</pre></details>
- [ ] Send your work url to workshop chat
	<details><summary>Hints</summary>
	<pre>👍</pre></details>
